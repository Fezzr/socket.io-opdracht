# README #

Dit is een begin voor een makkelijke opdracht met socket.io om te begrijpen hoe makkelijk en handig socket.io/websockets zijn.

### Opstarten ###

* Clone de repo op je pc ```git clone https://Fezzr@bitbucket.org/Fezzr/socket.io-opdracht.git``` of download hem via [Downloads](https://bitbucket.org/Fezzr/socket.io-opdracht/downloads/)
* Installeer [nodejs](https://nodejs.org/en/)
* Open een command line interface (cli/cmd)
* Zorg dat je cli in de map is van deze repo
* Draai ```npm update```
* start de node server in je cli ```node node.js```
* ga naar de client.html in je browser
* nu doe de opdracht, zorg dat je berichten naar elkaar kan versturen
* **[BONUS]** *geef elke gebruiker (mbv [robo hash](https://robohash.org/) en het socket id*

### Leidraad om deze opdracht te maken ###
Nu je de opdracht opgezet hebt en een kleine uitleg hebt gehad hoe socket.io werkt kan je proberen deze chat werkend te maken. 
De bedoeling van de opdracht is dat je iedereen die verbind met je socket server berichten naar elkaar kan versturen.

Als je nog steeds geen idee hebt kan je deze lijst aanhouden:

* haal het bericht dat de persoon heeft getypt op in de functie 'sendMessage'
* zorg dat je eigen bericht (mbv javascript) toegevoegd wordt aan de jouw chat
* 'emit' het bericht naar de server
* zorg dat de server kan luisteren('on) naar het bericht
* 'broadcast' het bericht naar alle andere gebruikers
* zorg dat de client luistert naar de net gebroadcaste bericht en zorg dat hij (mbv javascript) toegevoegd wordt