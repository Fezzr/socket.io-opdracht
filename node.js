var port = 8100;
var io = require('socket.io')(port);
console.log('socket server started on port', port);

io.on('connection', function(socket) {
    console.log(socket.id + ' connected to the server!');

});